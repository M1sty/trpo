﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
namespace School_SSS
{
    class DB
    {
        SqlConnection sqlcon = new SqlConnection();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        DataSet dataset = new DataSet();
        private static string login = "admin";
        private static string password = "admin";
        public static string returnLogin()
        {
            return login;
        }
        public static string returnPassword ()
        {
            return password;
        }
      
        public string connect()
        {
            return @"Data Source =localhost;Initial Catalog = School_Rudenko; Integrated Security = true";
        }

        public object getData(string tableName)
        {
            using (SqlConnection connection = new SqlConnection(connect()))
            {
                string query = "select * from " + tableName;
                connection.Open();
                SqlDataAdapter SqlDataAdapter = new SqlDataAdapter(query, connection);
                DataSet dataset = new DataSet();
                SqlDataAdapter.Fill(dataset);
                return dataset.Tables[0];
            }   
        }
        public object getInfo(string tableName, string condition)
        {
            using (SqlConnection connection = new SqlConnection(connect()))
            {
                string query = $"select * from {tableName} " + condition;
                connection.Open();
                SqlDataAdapter SqlDataAdapter = new SqlDataAdapter(query, connection);
                DataSet dataset = new DataSet();    
                SqlDataAdapter.Fill(dataset);
                return dataset.Tables[0];
            }
        }
        public object insertInfo(string tableName, string condition)
        {
            using (SqlConnection connection = new SqlConnection(connect()))
            {
                string query = $"insert into  {tableName} values ({condition})";
                connection.Open();
                SqlDataAdapter SqlDataAdapter = new SqlDataAdapter(query, connection);
                DataSet dataset = new DataSet();
                SqlDataAdapter.Fill(dataset);
                return null;
            }
        }
        public object updateInfo(string tableName, string condition)
        {
            using (SqlConnection connection = new SqlConnection(connect()))
            {
                string query = $"update {tableName} set {condition}";
                connection.Open();
                SqlDataAdapter SqlDataAdapter = new SqlDataAdapter(query, connection);
                DataSet dataset = new DataSet();
                SqlDataAdapter.Fill(dataset);
                return null;
            }
        }
        public object deleteInfo(string tableName, string condition)
        {
            using (SqlConnection connection = new SqlConnection(connect()))
            {
                string query = $"delete from {tableName} where {condition}";
                connection.Open();
                SqlDataAdapter SqlDataAdapter = new SqlDataAdapter(query, connection);
                DataSet dataset = new DataSet();
                SqlDataAdapter.Fill(dataset);
                return null;
            }
        }
    }
}
