﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace School_SSS
{
    public partial class ZForm : Form
    {
        public ZForm()
        {
            InitializeComponent();
        }
        SqlConnection sqlcon = new SqlConnection();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        DataSet dataset = new DataSet();
        DB db = new DB();
        string lf;
        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void ZForm_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = db.getData("getTeachers");
            using (SqlConnection connection = new SqlConnection(db.connect()))
            {
                string query = "select Наименование from Дисциплины";

                SqlDataAdapter SqlDataAdapter = new SqlDataAdapter(query, connection);
                DataSet dataset = new DataSet();
                SqlDataAdapter.Fill(dataset);
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    comboBox2.Items.Add(dataset.Tables[0].Rows[i][0]);
                    comboBox5.Items.Add(dataset.Tables[0].Rows[i][0]);
                }
                comboBox2.SelectedIndex = 0;

                 query = "select Наименование from Кабинеты ";

                SqlDataAdapter = new SqlDataAdapter(query, connection);
                 dataset = new DataSet();
                SqlDataAdapter.Fill(dataset);
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    comboBox3.Items.Add(dataset.Tables[0].Rows[i][0]);
                    comboBox6.Items.Add(dataset.Tables[0].Rows[i][0]);
                }
                comboBox3.SelectedIndex = 0;

                query = "select Наименование from Классы ";

                SqlDataAdapter = new SqlDataAdapter(query, connection);
                dataset = new DataSet();
                SqlDataAdapter.Fill(dataset);
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    comboBox4.Items.Add(dataset.Tables[0].Rows[i][0]);
                    comboBox7.Items.Add(dataset.Tables[0].Rows[i][0]);
                }
                comboBox4.SelectedIndex = 0;

                query = "select * from getTeachers";

                SqlDataAdapter = new SqlDataAdapter(query, connection);
                dataset = new DataSet();
                SqlDataAdapter.Fill(dataset);
                
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    comboBox1.Items.Add(dataset.Tables[0].Rows[i][1] + " " + dataset.Tables[0].Rows[i][2] + " " + dataset.Tables[0].Rows[i][3]);
                  
                }
                comboBox1.SelectedIndex = 0;
            }

           
        }

        private void textBox7_KeyUp(object sender, KeyEventArgs e)
        {
            if(textBox7.Text == "")
            {
                dataGridView1.DataSource = db.getData("getTeachers");
            }
            else
            {
               dataGridView1.DataSource =  db.getInfo("getTeachers", $"where Фамилия like '%{textBox7.Text}%'");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() != "" && textBox2.Text.Trim() != "" && textBox3.Text.Trim() != "" &&
                maskedTextBox1.Text.Trim() != "" && maskedTextBox2.Text.Trim() != "" && maskedTextBox3.Text.Trim() != "" && comboBox2.SelectedIndex != 0 &&
                comboBox3.SelectedIndex != 0)
            {
                try
                {
                    db.insertInfo("Сотрудники", $" '{textBox1.Text}','{textBox2.Text}', '{textBox3.Text}'," +
                    $"'{maskedTextBox1.Text}', '{maskedTextBox2.Text}', '{maskedTextBox3.Text}', '2'");                 
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                try
                {
                    if (comboBox4.SelectedIndex == 0)
                    {
                        db.insertInfo("Преподаватели", $"(select max(ID_Сотрудника) from Сотрудники),'{comboBox2.SelectedIndex}'," +
                     $"'{comboBox3.SelectedIndex}',NULL");
                    }
                    else
                    {
                        db.insertInfo("Преподаватели", $"(select max(ID_Сотрудника) from Сотрудники),'{comboBox2.SelectedIndex}'," +
                     $"'{comboBox3.SelectedIndex}','{comboBox4.SelectedIndex}'");
                    }
                    MessageBox.Show("Сотрудник успешно добавлен", "Добавление сотрудника", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dataGridView1.DataSource = db.getData("getTeachers");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Не заполнены необходимые поля!", "Ошибка добавления", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(db.connect()))
            {
               string query = "select * from getTeachers where Номер_сотрудника = " + comboBox1.SelectedIndex;
               
                SqlDataAdapter SqlDataAdapter = new SqlDataAdapter(query, connection);
               DataSet dataset = new DataSet();
                SqlDataAdapter.Fill(dataset);
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    textBox11.Text = dataset.Tables[0].Rows[i][1].ToString();
                    lf = textBox11.Text;
                }
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            db.updateInfo("getTeachers", $"Фамилия = '{textBox11.Text}' where Фамилия = '{lf}'");
            db.updateInfo("Преподаватели", $"ID_Дисциплины = '{comboBox5.SelectedIndex}' where ID_Сотрудника = '{comboBox1.SelectedIndex}'");
            db.updateInfo("Преподаватели", $"ID_Кабинета = '{comboBox6.SelectedIndex}' where ID_Сотрудника  = '{comboBox1.SelectedIndex}'");
            if (comboBox7.SelectedIndex != 0)
            {
                db.updateInfo("Преподаватели", $"ID_Класса = '{comboBox7.SelectedIndex}' where ID_Сотрудника  = '{comboBox1.SelectedIndex}'");
            }
            else
            {
                db.updateInfo("Преподаватели", $"ID_Класса = NULL where ID_Сотрудника  = '{comboBox1.SelectedIndex}'");
            }
            dataGridView1.DataSource = db.getData("getTeachers");
            MessageBox.Show("Сотрудник успешно изменен", "Изменение информации сотрудника", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox4.Text.Trim() != "")
            {
                db.deleteInfo("Преподаватели", $"ID_Сотрудника = {textBox4.Text}");
                db.deleteInfo("Сотрудники", $"ID_Сотрудника = {textBox4.Text}");
                MessageBox.Show("Сотрудник успешно удален", "Удаление сотрудника", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dataGridView1.DataSource = db.getData("getTeachers");
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {
            UForm uf = new UForm();
            this.Hide();
            uf.Show();
        }

        private void label19_Click(object sender, EventArgs e)
        {
            OForm of = new OForm();
            this.Hide();
            of.Show();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            Auth auth = new Auth();
            this.Hide();
            auth.Show();
        }

        private void groupBox2_Enter_1(object sender, EventArgs e)
        {

        }

        private void ZForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char l = e.KeyChar;
            if ((l < 'А' || l > 'я') && l != '\b' && l != '.')
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            char l = e.KeyChar;
            if ((l < 'А' || l > 'я') && l != '\b' && l != '.')
            {
                e.Handled = true;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            char l = e.KeyChar;
            if ((l < 'А' || l > 'я') && l != '\b' && l != '.')
            {
                e.Handled = true;
            }
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            char l = e.KeyChar;
            if ((l < 'А' || l > 'я') && l != '\b' && l != '.')
            {
                e.Handled = true;
            }
        }

        private void textBox4_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number))
            {
                e.Handled = true;
            }

        }
    }
}
    
