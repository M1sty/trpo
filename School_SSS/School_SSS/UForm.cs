﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace School_SSS
{
    public partial class UForm : Form
    {
        public UForm()
        {
            InitializeComponent();
        }
        SqlConnection sqlcon = new SqlConnection();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        DataSet dataset = new DataSet();
        DB db = new DB();
        string lf;
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() != "" && textBox2.Text.Trim() != "" && textBox3.Text.Trim() != ""
                && maskedTextBox1.Text.Trim() != "" && comboBox2.SelectedIndex != 0)
            {
               try
                {
                    db.insertInfo("Ученики", $"'{textBox1.Text}','{textBox2.Text}','{textBox3.Text}','{maskedTextBox1.Text}','{comboBox2.SelectedIndex}'");
                    MessageBox.Show("Ученик успешно добавлен", "Добавление ученика", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex )
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void UForm_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = db.getData("getApprentice");
            using (SqlConnection connection = new SqlConnection(db.connect()))
            {
                string query = "select Наименование from Классы";

                SqlDataAdapter SqlDataAdapter = new SqlDataAdapter(query, connection);
                DataSet dataset = new DataSet();
                SqlDataAdapter.Fill(dataset);
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    comboBox3.Items.Add(dataset.Tables[0].Rows[i][0]);
                    comboBox2.Items.Add(dataset.Tables[0].Rows[i][0]);
                }
                comboBox2.SelectedIndex = 0;
                comboBox3.SelectedIndex = 0;
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(db.connect()))
            {
                string query = "select * from getApprentice where Номер_ученика = " + comboBox1.SelectedIndex;

                SqlDataAdapter SqlDataAdapter = new SqlDataAdapter(query, connection);
                DataSet dataset = new DataSet();
                SqlDataAdapter.Fill(dataset);
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    textBox11.Text = dataset.Tables[0].Rows[i][1].ToString();
                    lf = textBox11.Text;    
                }

            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            ZForm zf = new ZForm();
            this.Hide();
            zf.Show();

        }

        private void label9_Click(object sender, EventArgs e)
        {
            OForm of = new OForm();
            this.Hide();
            of.Show();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            Auth auth = new Auth();
            this.Hide();
            auth.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            db.deleteInfo("Четвертные_отметки", $"ID_Ученика = {textBox4.Text}");
            db.deleteInfo("Ученики", $"ID_Ученика = {textBox4.Text}");
            MessageBox.Show("Ученик успешно удален", "Удаление ученика", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            db.updateInfo("getApprentice", $"Фамилия = '{textBox11.Text}' where Фамилия = '{lf}'");
            db.updateInfo("Ученики", $"ID_Класса = '{textBox11.Text}' where ID_Ученика = '{comboBox3.SelectedIndex}'");
            MessageBox.Show("Ученик успешно изменен", "Изменение ученика", MessageBoxButtons.OK, MessageBoxIcon.Information);
            dataGridView1.DataSource = db.getData("getApprentice");
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_KeyUp(object sender, KeyEventArgs e)
        {
            if (textBox7.Text == "")
            {
                dataGridView1.DataSource = db.getData("getApprentice");
            }
            else
            {
                dataGridView1.DataSource = db.getInfo("getApprentice", $"where Фамилия like '%{textBox7.Text}%'");
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void UForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char l = e.KeyChar;
            if ((l < 'А' || l > 'я') && l != '\b' && l != '.')
            {
                e.Handled = true;
            }
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            char l = e.KeyChar;
            if ((l < 'А' || l > 'я') && l != '\b' && l != '.')
            {
                e.Handled = true;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            char l = e.KeyChar;
            if ((l < 'А' || l > 'я') && l != '\b' && l != '.')
            {
                e.Handled = true;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            char l = e.KeyChar;
            if ((l < 'А' || l > 'я') && l != '\b' && l != '.')
            {
                e.Handled = true;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number))
            {
                e.Handled = true;
            }

        }
    }
}
