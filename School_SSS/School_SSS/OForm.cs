﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace School_SSS
{
    public partial class OForm : Form
    {
        public OForm()
        {
            InitializeComponent();
        }
        SqlConnection sqlcon = new SqlConnection();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        DataSet dataset = new DataSet();
        DB db = new DB();
        string lf;
        private void OForm_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = db.getData("getJournal");
            using (SqlConnection connection = new SqlConnection(db.connect()))
            {
                string query = "select Наименование from Дисциплины";

                SqlDataAdapter SqlDataAdapter = new SqlDataAdapter(query, connection);
                DataSet dataset = new DataSet();
                SqlDataAdapter.Fill(dataset);
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    comboBox1.Items.Add(dataset.Tables[0].Rows[i][0]);
                }
                comboBox1.SelectedIndex = 0;

 

                query = "select * from getApprentice";

                SqlDataAdapter = new SqlDataAdapter(query, connection);
                dataset = new DataSet();
                SqlDataAdapter.Fill(dataset);

                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    comboBox2.Items.Add(dataset.Tables[0].Rows[i][1] + " " + dataset.Tables[0].Rows[i][2] + " " + dataset.Tables[0].Rows[i][3]);

                }
                comboBox2.SelectedIndex = 0;

                query = "select * from Четвертные_отметки";

                SqlDataAdapter = new SqlDataAdapter(query, connection);
                dataset = new DataSet();
                SqlDataAdapter.Fill(dataset);

                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    comboBox6.Items.Add(dataset.Tables[0].Rows[i][0]);

                }
                comboBox6.SelectedIndex = 0;
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {
            Auth auth = new Auth();
            this.Hide();
            auth.Show();
        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {
            UForm uf = new UForm();
            this.Hide();
            uf.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            ZForm zf = new ZForm();
            this.Hide();
            zf.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBox6.SelectedIndex != 0 && comboBox4.SelectedIndex != 0)
            {
                db.updateInfo("Четвертные_отметки", $"Отметка = '{comboBox4.SelectedItem.ToString()}' where ID_Отметки = '{comboBox6.SelectedItem.ToString()}'");
                MessageBox.Show("Оценка успешно изменена", "Изменение оценки", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dataGridView1.DataSource = db.getData("getJournal");
            }
            else
            {
                MessageBox.Show("Заполните все поля", "Изменение оценки", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(comboBox1.SelectedIndex != 0 && comboBox2.SelectedIndex != 0 && comboBox3.SelectedIndex != 0)
            {
                db.insertInfo("Четвертные_отметки", $"'{comboBox1.SelectedIndex}','{comboBox2.SelectedIndex}','{comboBox3.SelectedItem.ToString()}'");
                MessageBox.Show("Оценка успешно добавлена", "Добавление оценки", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dataGridView1.DataSource = db.getData("getJournal");
            }
            else
            {
                MessageBox.Show("Заполните все поля", "Добавление оценки", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
