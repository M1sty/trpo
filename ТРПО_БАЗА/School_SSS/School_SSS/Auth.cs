﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace School_SSS
{
    public partial class Auth : Form
    {
        public Auth()
        {
            InitializeComponent();
        }
        SqlConnection sqlcon = new SqlConnection();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        DataSet dataset = new DataSet();
        DB db = new DB();
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == DB.returnLogin() && textBox2.Text == DB.returnPassword())
            {
                using (SqlConnection connection = new SqlConnection(db.connect()))
                {
                    string query = "select Фамилия,Имя,Отчество from Сотрудники";
                    connection.Open();
                    SqlDataAdapter SqlDataAdapter = new SqlDataAdapter(query, connection);
                    DataSet dataset = new DataSet();
                    SqlDataAdapter.Fill(dataset);
                    
                    MessageBox.Show($"Здравствуйте, {dataset.Tables[0].Rows[0][0]} {dataset.Tables[0].Rows[0][1]} {dataset.Tables[0].Rows[0][2]}");
                    ZForm zForm = new ZForm();
                    this.Hide();
                    zForm.Show();                    
                }
            }
            else
            {
                MessageBox.Show("Неверный логин или пароль", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
