use School_Rudenko
go

create alter trigger checkClass
on �������������
for insert,update
as
declare @idEmp int,@idClass int, @countT int,@countE int,@countEMax int,@countTMax int
select @countT = ( (select count(ID_�������������) from �������������) - 1)
select @countE = ( (select count(ID_����������) from ����������) - 1)
select @countEMax = ( (select count(ID_����������) from ����������))
select @countTMax = ( (select count(ID_�������������) from �������������))
select @idEmp = (select ID_������������� from inserted)
select @idClass = (select ID_������ from ������������� where ID_������������� = @idEmp)
begin
if ( (select count(ID_������) from ������������� where ID_������ = @idClass) > 1 )
	begin
		raiserror('������ ����� ��� �����',0,1)
		rollback transaction
		execute updateIdent '�������������',@countT
	end
	if (@countEMax > @countE and @countTMax > @countT)
		begin
			execute updateIdent '�������������',@countTMax
			execute updateIdent '����������',@countEMax
			raiserror('111',0,1)
		end 
end	
go

create alter trigger checkIdent
on ����������_������� 
for delete
as
declare @count int
select @count = ( (select count(ID_�������) from ����������_�������) - 1)
begin
	execute updateIdent '����������_������',@count
end
go

create alter  trigger checkIdent1
on �������
for delete
as
declare @count int
select @count = ( (select count(ID_�������) from �������) - 1)
begin
	execute updateIdent '�������',@count
end
go

create drop trigger checkIdent2
on ����������
for delete
as
declare @count int
select @count = ( (select count(ID_����������) from ����������) - 1)
begin
		execute updateIdent '����������',@count
end
go 

create alter trigger checkIdent3
on �������������
after delete
as
declare @countT int,@countE int
select @countT = ( (select count(ID_�������������) from �������������) - 1)
select @countE = ( (select count(ID_�������������) from �������������) - 1)
begin
	if (@countE = 0 and @countT  = 0)
	begin	
		execute updateIdent '�������������',1
		execute updateIdent '����������',1
	end
	else 
	begin
		execute updateIdent '�������������',@countT
		execute updateIdent '����������',@countE
	end

end
go

create procedure updateIdent 
@name varchar(30),@count int
as
begin
	DBCC CHECKIDENT(@name,RESEED,@count)
end
go